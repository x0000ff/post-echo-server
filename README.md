# Node.js Echo server with POST

> Thanks to **Frank Grimm**  
[http://blog.frankgrimm.net/2010/11/howto-access-http-message-body-post-data-in-node-js/](http://blog.frankgrimm.net/2010/11/howto-access-http-message-body-post-data-in-node-js/)  
>  
> **Gist**:  
[https://gist.github.com/FrankGrimm/669233](https://gist.github.com/FrankGrimm/669233)

## Run
```terminal
$ npm start
```

#Usage

```terminal
curl --request POST \
  --url http://localhost:3000/jsonecho \
  --data 'simple_text=DEADBEEF&email=user%40server.com&all_characters=%601234567890-%3D~!%40%23%24%25%5E%26*()_%2B%5B%5D%5C%3B'\''%2C.%2FQWERTYUIOP%7B%7D%7CASDFGHJKL%3A%22ZXCVBNM%3C%3E%3F&empty=&with%20space%20key=with%20space%20value'
```

## Response

```
[xecho]$ npm start

> xecho@0.0.1 start /Users/x0000ff/Dev/node.js/xecho
> node server.js

Server started at port 3000
[200] POST to /jsonecho

------------------------

RAW:
simple_text=DEADBEEF&email=user%40server.com&all_characters=%601234567890-%3D~!%40%23%24%25%5E%26*()_%2B%5B%5D%5C%3B'%2C.%2FQWERTYUIOP%7B%7D%7CASDFGHJKL%3A%22ZXCVBNM%3C%3E%3F&empty=&with+space+key=with+space+value

------------------------

DECODED:
{ simple_text: 'DEADBEEF',
  email: 'user@server.com',
  all_characters: '`1234567890-=~!@#$%^&*()_+[]\\;\',./QWERTYUIOP{}|ASDFGHJKL:"ZXCVBNM<>?',
  empty: '',
  'with space key': 'with space value' }

------------------------


```

